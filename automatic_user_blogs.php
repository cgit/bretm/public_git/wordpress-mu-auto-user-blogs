<?php
/*
Plugin Name: Automatic User Blogs
Plugin URI: http://www.redhat.com/
Description: A simplistic plugin to automatically create personal blogs for new users
Version: 0.1
Author: Bret McMillan (bretm@redhat.com)

Copyright (C) 2008 Red Hat, Inc.

This is free software, licensed to you under the GNU General Public
License, version 2 (GPLv2). A copy of GPLv2 is available at 
http://www.gnu.org/licenses/old-licenses/gpl-2.0.html .

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

if (! class_exists('AutoUserBlogPlugin')) {

  class AutoUserBlogPlugin {

    function AutoUserBlogPlugin() {
      add_action('init', array(&$this, 'init'));
      add_action('wpmu_new_user', array(&$this, 'create_newuser_blog'));
      add_action('admin_menu', array(&$this, 'admin_menu'));
    }

    function init() {
      if ( !get_site_option('auto_blog_create') && current_user_can('manage_options')) {
	add_site_option('auto_blog_create', True, 'Create a personal blog automatically for new users?');
      }
    }

    function admin_menu() {
      $cur_user = wp_get_current_user();
      $cached_user = wp_cache_get($cur_user->ID, 'users');
      if (function_exists('add_submenu_page') &&
	  is_site_admin($cached_user->user_login)) {
	add_submenu_page('wpmu-admin.php', 'Automatic User Blogs', 'Automatic User Blogs', 9, basename(__FILE__), array(&$this, 'render_options'));
      }
    }

    function render_options() {
      if( ! is_site_admin() ) {
	wp_die( __('<p>You do not have permission to access this page.</p>') );
      }

      if ($_POST['newuserautoblogOptionsSave']) {
	update_site_option('auto_blog_create', $_POST['auto_blog_create']);
        ?>
	  <div id="message" class="updated fade"><p><?php _e('Options saved!'); ?></p></div><?php
      }
      $autocreate = get_site_option('auto_blog_create');
?>
<div class="wrap">
  <h2>Automatic User Blog Options</h2>
  <form method="post">
    <input type="hidden" name="action" value="update" />
    <input type="hidden" name="page_options" value="auto_blog_create" />
    <?php if (function_exists('wp_nonce_field')): wp_nonce_field('update-options'); endif; ?>

    <fieldset class="options">
      <table class="editform optiontable">
        <tr valign="top">
          <th scope="row"><label for="auto_blog_create">Create personal blog on initial login?</label></th>
          <td>
            <input type="checkbox" name="auto_blog_create" id="auto_blog_create" checked="<?php if ($autocreate) echo 'checked' ?>" />
          </td>
        </tr>
      </table>
    </fieldset>
    <p class="submit">
      <input type="submit" name="newuserautoblogOptionsSave" value="Update Options &raquo;" />
    </p>
  </form>
</div>
<?php
    }

    function create_newuser_blog( $user_id ) {
      global $wpdb;

      $user = new WP_User($user_id);

      // need to make this more configurable?
      $principal = preg_split('/\@/', $user->user_login, -1, PREG_SPLIT_NO_EMPTY);
      $username = $principal[0];

      $site = get_current_site();

      wpmu_create_blog($site->domain, '/' . $username, $user->first_name . "'s Blog", $user_id);

      return true;
    }
  }

  $auto_user_blog_plugin = new AutoUserBlogPlugin();
 }
?>
